if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper fslversion update_fsl_release update_fsl_package open_fsl_report find_cuda_exe
fi
